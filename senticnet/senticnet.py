# -*- coding: UTF-8 -*-
'''
Created on: Apr 29, 2015
    Author: Johnnatan Messias
    johnnatan@dcc.ufmg.br
'''
import re, sys, os



def regex(text, stopwords):
    tokens = re.findall(r"[a-zA-Z]+-[a-zA-Z]+|[a-zA-Z]+", text.lower())
    return [token for token in tokens if token not in stopwords]
     
def checkText(textList):
    results = []
    for tokens in textList:
        features = {'pleasantness' : 0, 'attention' : 0, 'sensitivity' : 0, 'aptitude': 0, 'polarity' : 0}
        if(len(tokens) == 0):
            #results.append(features)
            results.append(features['polarity'])
            continue
        index = 3
        ngram = 0
        #print tokens
        while(index - 3 < len(tokens)):
            if((len(tokens) > index) and dataset.has_key(tokens[index - 3] + " " + tokens[index - 2] + " " + tokens[index - 1] + " " + tokens[index])):
                #4-gram
                token = tokens[index - 3] + " " + tokens[index - 2] + " " + tokens[index - 1] + " " + tokens[index]
                index += 3
                ngram += 3
            elif((len(tokens) > index - 1) and dataset.has_key(tokens[index - 3] + " " + tokens[index - 2] + " " + tokens[index - 1])):
                #3-gram
                token = tokens[index - 3] + " " + tokens[index - 2] + " " + tokens[index - 1]
                index += 2
                ngram += 2
            elif((len(tokens) > index - 2) and dataset.has_key(tokens[index - 3] + " " + tokens[index - 2])):
                #2-gram
                token = tokens[index - 3] + " " + tokens[index - 2]
                index += 1
                ngram += 1
            elif(dataset.has_key(tokens[index - 3])):
                #1-gram
                token = tokens[index - 3]
            else:
                index += 1
                continue
            index += 1
            #print token
            features['pleasantness'] += dataset[token]['pleasantness']
            features['attention'] += dataset[token]['attention']
            features['sensitivity'] += dataset[token]['sensitivity']
            features['aptitude'] += dataset[token]['aptitude']
            features['polarity'] += dataset[token]['polarity']

        results.append(features['polarity'] / float(len(tokens) - ngram))

    return results

def loadTextInput(text):
    return [regex(text, stopwords)]

def loadText(filenameIn):
    textFile = open(filenameIn, 'r')
    text = [regex(line, stopwords) for line in textFile.readlines()]
    textFile.close()
    return text

def loadDataset(filenameIn):
    dataset = {}
    datasetFile = open(filenameIn, 'r')
    line = datasetFile.readline().strip()
    while(line):
        line = line.split('\t')
        dataset[line[0].split('=')[1]] = {'pleasantness' : float(line[1].split('=')[1]), 'attention' : float(line[2].split('=')[1]), 'sensitivity' : float(line[3].split('=')[1]), 'aptitude' : float(line[4].split('=')[1]), 'polarity' : float(line[5].split('=')[1])}
        line = datasetFile.readline().strip()
    datasetFile.close()
    return dataset

def loadStopWords(filenameIn):
     return open(filenameIn, 'r').readline().strip().split(',')

def saveFile(results, filenameOut):
    outFile = open(filenameOut, 'w')
    for result in results:
        outFile.write("%lf\n" %result)
    outFile.close()

def printResults(results):
    for result in results:
        print ("%lf" %result)


CURRENT_FILE_PATH = os.path.dirname(__file__)
if CURRENT_FILE_PATH != "":
    CURRENT_FILE_PATH += "/"

filenameStopWords = CURRENT_FILE_PATH + 'stopwords.txt'
filenameDataset = CURRENT_FILE_PATH + 'senticnet_dataset.tsv'
filenameResult =  CURRENT_FILE_PATH + 'out.txt'
textList = ""
stopwords = loadStopWords(filenameStopWords)
dataset = loadDataset(filenameDataset)

def execute():
    argc = len(sys.argv)
    argv = sys.argv
    if(argc > 2):
        if(argv[1] == '-t'):
            textList = loadTextInput(argv[2])
        elif(argv[1] == '-f'):
            textList = loadText(argv[2])
    else:
        print("Bad input format!")
        exit()
    results = checkText(textList)
    saveFile(results, filenameResult)
    printResults(results)

if __name__ == "__main__":
    execute()
