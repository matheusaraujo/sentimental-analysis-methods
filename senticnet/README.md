### Senticnet v3.0 ###

### C++ ###
make

For a text input use:
./senticnet -t "YourTextHere"

For a file:
./senticnet -f "YourFileHere"

Example:
Text:
./senticnet -t "love"

File:
./senticnet -f "../test"

### Python ###
For a text input use:
python senticnet.py -t "YourTextHere"

For a file:
python senticnet.py -f "YourFileHere"

Example:
Text:
python senticnet.py -t "love"

File:
python senticnet.py -f "../test"


