######
---------
output/1.txt
---------
Text Length: 3
---------
Nouns:
-----
love 3.0  = 3.0
-----
Average SO: 3.0
-----
Verbs:
-----
-----
Average SO: 0
-----
Adjectives:
-----
-----
Average SO: 0
-----
Adverbs:
-----
-----
Average SO: 0
-----
-----
SO by Sentence
-----
i love you 3.0
---------
Total SO: 3.0
---------
######
---------
output/3.txt
---------
Text Length: 3
---------
Nouns:
-----
-----
Average SO: 0
-----
Verbs:
-----
-----
Average SO: 0
-----
Adjectives:
-----
-----
Average SO: 0
-----
Adverbs:
-----
-----
Average SO: 0
-----
-----
SO by Sentence
-----
i want you 0
---------
Total SO: 0
---------
######
---------
output/2.txt
---------
Text Length: 3
---------
Nouns:
-----
-----
Average SO: 0
-----
Verbs:
-----
hate -4.0  X 1.5 (NEGATIVE) = -6.0
-----
Average SO: -6.0
-----
Adjectives:
-----
-----
Average SO: 0
-----
Adverbs:
-----
-----
Average SO: 0
-----
-----
SO by Sentence
-----
i hate you -6.0
---------
Total SO: -6.0
---------
######
---------
output/4.txt
---------
Text Length: 4
---------
Nouns:
-----
-----
Average SO: 0
-----
Verbs:
-----
really love 3.0 X 1.2 (INTENSIFIED)  = 3.6
-----
Average SO: 3.6
-----
Adjectives:
-----
-----
Average SO: 0
-----
Adverbs:
-----
-----
Average SO: 0
-----
-----
SO by Sentence
-----
I really love you 3.6
---------
Total SO: 3.6
---------
