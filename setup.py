import os

import subprocess

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

user_input = ""
while user_input != "y" and user_input != "n":
  user_input = raw_input("Are you on Debian distribution?[y][n]")
if user_input == "y":
  cmd = ["sudo","apt-get","install","python-setuptools", "build-essential",
         "python-dev","libxslt1-dev","libxml2-dev"]
  subprocess.call(cmd)
  cmd = ["sudo","easy_install","pip"]
  subprocess.call(cmd)
  cmd = ["sudo","pip","install","virtualenv"]
  subprocess.call(cmd)
  java_call = subprocess.call("java",shell=True)
  if java_call == 127:
	cmd = ["sudo","apt-get","install","openjdk-7-jdk"]
	subprocess.call(cmd)

else:
  print """
  You should install these packages equivalent:
    sudo apt-get install python-setuptools
    sudo apt-get install build-essential
    sudo apt-get install python-dev
    sudo apt-get install libxslt1-dev
    sudo apt-get install libxml2-dev
    sudo easy_install pip
    sudo pip install virtualenv
    sudo apt-get install openjdk-7
  """




from setuptools import setup, find_packages

print "Install Perl dependencies(local/lib.pm):"
cmd = ['sudo', 'cpan', 'local/lib.pm']
subprocess.call(cmd)
print "Install Perl dependencies(Lingua/StopWords.pm):"
cmd = ['sudo', 'cpan', 'Lingua/StopWords.pm']
subprocess.call(cmd)
print "Install Perl dependencies(HTML/Filter.pm):"
cmd = ['sudo', 'cpan', 'HTML/Filter.pm']
subprocess.call(cmd)
print "Install Numpy in this Python:"
cmd = ['pip', 'install', 'numpy']
subprocess.call(cmd)
print "Install specific setuptools in this Python:"
cmd = ['pip', 'install', 'setuptools==9.1']
subprocess.call(cmd)
print "Install NLTK 2.0.1 in this Python:"
cmd = ['pip', 'install', 'nltk==2.0.1']
subprocess.call(cmd)
print "Install chardet in this Python:"
cmd = ['pip', 'install', 'chardet==2.2.1']
subprocess.call(cmd)
print "Install sasa in this Python:"
cmd = ['pip', 'install', 'sasa==0.1.3']
subprocess.call(cmd)
print "Install lxml in this Python:"
cmd = ['pip', 'install', 'lxml==3.3.5']
subprocess.call(cmd)
print "Install TwitterSearch in this Python:"
cmd = ['pip', 'install', 'TwitterSearch==0.78.4']
subprocess.call(cmd)
print "Install pyramid_mailer in this Python:"
cmd = ['pip', 'install', 'pyramid_mailer==0.13']
subprocess.call(cmd)
print "Install SQLAlchemy in this Python:"
cmd = ['pip', 'install', 'SQLAlchemy==0.9.6']
subprocess.call(cmd)
print "Install pyramid in this Python:"
cmd = ['pip', 'install', 'pyramid==1.5.1']
subprocess.call(cmd)
print "Install vader in this Python:"
cmd = ['pip', 'install', 'vaderSentiment==0.5']
subprocess.call(cmd)
print "Install stem in this Python:"
cmd = ['pip', 'install', 'stem==1.4.0']
subprocess.call(cmd)
print "Install stemming in this Python:"
cmd = ['pip', 'install', 'stemming==1.0.1']
subprocess.call(cmd)
print "Generate locale pt_BR:"
cmd = ['sudo','locale-gen', 'pt_BR', 'pt_BR.UTF-8']
subprocess.call(cmd)
print "Install pattern in this Python:"
cmd = ['pip', 'install', 'pattern==2.6']
subprocess.call(cmd)
print "Install nltk in this python:"
cmd = ['pip', 'install', 'nltk==2.0.1']
subprocess.call(cmd)



requires = [
    'mysql-python==1.2.5',
    'pyramid==1.5.1',
    'SQLAlchemy==0.9.6',
    'transaction==1.4.3',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'zope.sqlalchemy==0.7.5',
    'waitress==0.8.9',
    'pyramid_mako==1.0.2',
    'pyramid_chameleon==0.3',
    'pyramid_mailer==0.13',
    'xlwt==0.7.5',
    'TwitterSearch==0.78.4',
    'lxml==3.3.5',
    'nltk==2.0.1',
    'sasa==0.1.3',
    'chardet==2.2.1',
    'BeautifulSoup==3.2.1',
    'cornice==0.17',
    "vaderSentiment==0.5",
    "stem==1.4.0",
    "stemming==1.0.1",
    ]

import importlib
nltk = importlib.import_module('nltk')

nltk.download('stopwords')
nltk.download('maxent_treebank_pos_tagger')
nltk.download('punkt')
