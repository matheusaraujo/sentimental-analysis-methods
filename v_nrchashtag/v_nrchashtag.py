import nltk
import os
import sys
from optparse import OptionParser
#!/usr/bin/python
# coding: utf-8 
'''
Created on July 04, 2013
@author: C.J. Hutto

Citation Information

If you use any of the VADER sentiment analysis tools 
(VADER sentiment lexicon or Python code for rule-based sentiment 
analysis engine) in your work or research, please cite the paper. 
For example:

  Hutto, C.J. & Gilbert, E.E. (2014). VADER: A Parsimonious Rule-based Model for 
  Sentiment Analysis of Social Media Text. Eighth International Conference on 
  Weblogs and Social Media (ICWSM-14). Ann Arbor, MI, June 2014.
'''

#
#   Matheus modification to work with unigram and bigrams and vader based on Joao Paulo Implementation
#   

import os, math, re, sys, fnmatch, string 
reload(sys)

threshold = 0.05

unigrams_lexicon = {} #key: a str representing an unigram; value: the unigram's score
bigrams_lexicon = {}  #key: a str representing a bigram, in format 'unigram<space>unigram'; values: the bigram's score
pairs_lexicon = {}    #key: a str representing a pair of non-contiguous ngrams, in format 'ngram---ngram', where ngram can be unigram or bigram; value: the pair's score
THRESHOLD_NEUTRAL = 0.5 #absolute value


def load_lexicons():
    load_lexicon(unigrams_lexicon, 'unigrams')
    load_lexicon(bigrams_lexicon, 'bigrams')

def load_lexicon(ngrams_lexicon, ngrams_file_prefix):
    tab_tokenizer = nltk.tokenize.TabTokenizer().tokenize
    database_filename = ngrams_file_prefix + "-pmilexicon.txt"
    with open(database_filename,"r") as file_ptr:
        for line in file_ptr:
            line = line.rstrip()
            tokens = tab_tokenizer(line)
            ngrams_lexicon[tokens[0]] = float(tokens[1])


def getVaderWordsOnlyWithBigrams(text):
    text  = text.lower()
    white_tokenizer = nltk.tokenize.WhitespaceTokenizer().tokenize
    words = white_tokenizer(text)
    score_total = 0.0

    unigrams_indexes_to_remove = set()
    bigrams_indexes_to_remove = set()

    unigrams = words;
    bigrams = nltk.bigrams(words)
    qtd_u = unigrams.__len__()
    qtd_b = bigrams.__len__()
    grams_in_order = unigrams[:]

    #bigrams
    i = 0
    for bigram in bigrams:
        if bigram != None:
            bigram_str = bigram[0] + ' ' + bigram[1]
            if bigrams_lexicon.has_key(bigram_str):
                for _j in range(len(grams_in_order) - 1):
                    if  grams_in_order[_j] == bigram[0] and grams_in_order[_j+1] == bigram[1]:
                        grams_in_order[_j] = bigram_str
                        grams_in_order[_j+1] = None
        i += 1

    grams_in_order = [x for x in grams_in_order if x]
    return grams_in_order

#TODO remover caracteres de pontuacao no final de cada token, por exemplo usando a ER [,.!?]

def make_lex_dict():
    final_lexicon = {}
    for item in unigrams_lexicon.iteritems():
        final_lexicon[item[0]] = item[1]
    for item in bigrams_lexicon.iteritems():
        final_lexicon[item[0]] = item[1]

    return final_lexicon

load_lexicons()    
word_valence_dict = make_lex_dict()

# for removing punctuation
regex_remove_punctuation = re.compile('[%s]' % re.escape(string.punctuation))

def sentiment(text):
    """
    Returns a float for sentiment strength based on the input text.
    Positive values are positive valence, negative value are negative valence.
    """
    wordsAndEmoticons = str(text).split() #doesn't separate words from adjacent punctuation (keeps emoticons & contractions)
    text_mod = regex_remove_punctuation.sub('', text) # removes punctuation (but loses emoticons & contractions)
    wordsOnly = getVaderWordsOnlyWithBigrams(text)
    # get rid of empty items or single letter "words" like 'a' and 'I' from wordsOnly
    for word in wordsOnly:
        if len(word) <= 1:
            wordsOnly.remove(word)    
    # now remove adjacent & redundant punctuation from [wordsAndEmoticons] while keeping emoticons and contractions
    puncList = [".", "!", "?", ",", ";", ":", "-", "'", "\"", 
                "!!", "!!!", "??", "???", "?!?", "!?!", "?!?!", "!?!?"] 
    for word in wordsOnly:
        for p in puncList:
            pword = p + word
            x1 = wordsAndEmoticons.count(pword)
            while x1 > 0:
                i = wordsAndEmoticons.index(pword)
                wordsAndEmoticons.remove(pword)
                wordsAndEmoticons.insert(i, word)
                x1 = wordsAndEmoticons.count(pword)
            
            wordp = word + p
            x2 = wordsAndEmoticons.count(wordp)
            while x2 > 0:
                i = wordsAndEmoticons.index(wordp)
                wordsAndEmoticons.remove(wordp)
                wordsAndEmoticons.insert(i, word)
                x2 = wordsAndEmoticons.count(wordp)
    # get rid of residual empty items or single letter "words" like 'a' and 'I' from wordsAndEmoticons
    for word in wordsAndEmoticons:
        if len(word) <= 1:
            wordsAndEmoticons.remove(word)
    
    # remove stopwords from [wordsAndEmoticons]
    #stopwords = [str(word).strip() for word in open('stopwords.txt')]
    #for word in wordsAndEmoticons:
    #    if word in stopwords:
    #        wordsAndEmoticons.remove(word)
    
    # check for negation
    negate = ["aint", "arent", "cannot", "cant", "couldnt", "darent", "didnt", "doesnt",
              "ain't", "aren't", "can't", "couldn't", "daren't", "didn't", "doesn't",
              "dont", "hadnt", "hasnt", "havent", "isnt", "mightnt", "mustnt", "neither",
              "don't", "hadn't", "hasn't", "haven't", "isn't", "mightn't", "mustn't",
              "neednt", "needn't", "never", "none", "nope", "nor", "not", "nothing", "nowhere", 
              "oughtnt", "shant", "shouldnt", "uhuh", "wasnt", "werent",
              "oughtn't", "shan't", "shouldn't", "uh-uh", "wasn't", "weren't",  
              "without", "wont", "wouldnt", "won't", "wouldn't", "rarely", "seldom", "despite"]
    def negated(list, nWords=[], includeNT=True):
        nWords.extend(negate)
        for word in nWords:
            if word in list:
                return True
        if includeNT:
            for word in list:
                if "n't" in word:
                    return True
        if "least" in list:
            i = list.index("least")
            if i > 0 and list[i-1] != "at":
                return True
        return False
        
    def normalize(score, alpha=15):
        # normalize the score to be between -1 and 1 using an alpha that approximates the max expected value 
        normScore = score/math.sqrt( ((score*score) + alpha) )
        return normScore
    
    def wildCardMatch(patternWithWildcard, listOfStringsToMatchAgainst):
        listOfMatches = fnmatch.filter(listOfStringsToMatchAgainst, patternWithWildcard)
        return listOfMatches
        
    
    def isALLCAP_differential(wordList):
        countALLCAPS= 0
        for w in wordList:
            if str(w).isupper(): 
                countALLCAPS += 1
        cap_differential = len(wordList) - countALLCAPS
        if cap_differential > 0 and cap_differential < len(wordList):
            isDiff = True
        else: isDiff = False
        return isDiff
    isCap_diff = isALLCAP_differential(wordsAndEmoticons)
    
    b_incr = 0.293 #(empirically derived mean sentiment intensity rating increase for booster words)
    b_decr = -0.293
    # booster/dampener 'intensifiers' or 'degree adverbs' http://en.wiktionary.org/wiki/Category:English_degree_adverbs
    booster_dict = {"absolutely": b_incr, "amazingly": b_incr, "awfully": b_incr, "completely": b_incr, "considerably": b_incr, 
                    "decidedly": b_incr, "deeply": b_incr, "effing": b_incr, "enormously": b_incr, 
                    "entirely": b_incr, "especially": b_incr, "exceptionally": b_incr, "extremely": b_incr,
                    "fabulously": b_incr, "flipping": b_incr, "flippin": b_incr, 
                    "fricking": b_incr, "frickin": b_incr, "frigging": b_incr, "friggin": b_incr, "fully": b_incr, "fucking": b_incr, 
                    "greatly": b_incr, "hella": b_incr, "highly": b_incr, "hugely": b_incr, "incredibly": b_incr, 
                    "intensely": b_incr, "majorly": b_incr, "more": b_incr, "most": b_incr, "particularly": b_incr, 
                    "purely": b_incr, "quite": b_incr, "really": b_incr, "remarkably": b_incr, 
                    "so": b_incr,  "substantially": b_incr, 
                    "thoroughly": b_incr, "totally": b_incr, "tremendously": b_incr, 
                    "uber": b_incr, "unbelievably": b_incr, "unusually": b_incr, "utterly": b_incr, 
                    "very": b_incr, 
                    
                    "almost": b_decr, "barely": b_decr, "hardly": b_decr, "just enough": b_decr, 
                    "kind of": b_decr, "kinda": b_decr, "kindof": b_decr, "kind-of": b_decr,
                    "less": b_decr, "little": b_decr, "marginally": b_decr, "occasionally": b_decr, "partly": b_decr, 
                    "scarcely": b_decr, "slightly": b_decr, "somewhat": b_decr, 
                    "sort of": b_decr, "sorta": b_decr, "sortof": b_decr, "sort-of": b_decr}
    sentiments = []
    for item in wordsAndEmoticons:
        v = 0
        i = wordsAndEmoticons.index(item)
        if (i < len(wordsAndEmoticons)-1 and str(item).lower() == "kind" and \
           str(wordsAndEmoticons[i+1]).lower() == "of") or str(item).lower() in booster_dict:
            sentiments.append(v)
            continue
        item_lowercase = str(item).lower() 
        if  item_lowercase in word_valence_dict:
            #get the sentiment valence
            v = float(word_valence_dict[item_lowercase])
            
            #check if sentiment laden word is in ALLCAPS (while others aren't)
            c_incr = 0.733 #(empirically derived mean sentiment intensity rating increase for using ALLCAPs to emphasize a word)
            if str(item).isupper() and isCap_diff:
                if v > 0: v += c_incr
                else: v -= c_incr
            
            #check if the preceding words increase, decrease, or negate/nullify the valence
            def scalar_inc_dec(word, valence):
                scalar = 0.0
                word_lower = str(word).lower()
                if word_lower in booster_dict:
                    scalar = booster_dict[word_lower]
                    if valence < 0: scalar *= -1
                    #check if booster/dampener word is in ALLCAPS (while others aren't)
                    if str(word).isupper() and isCap_diff:
                        if valence > 0: scalar += c_incr
                        else:  scalar -= c_incr
                return scalar
            n_scalar = -0.74
            if i > 0 and str(wordsAndEmoticons[i-1]).lower() not in word_valence_dict:
                s1 = scalar_inc_dec(wordsAndEmoticons[i-1], v)
                v = v+s1
                if negated([wordsAndEmoticons[i-1]]): v = v*n_scalar
            if i > 1 and str(wordsAndEmoticons[i-2]).lower() not in word_valence_dict:
                s2 = scalar_inc_dec(wordsAndEmoticons[i-2], v)
                if s2 != 0: s2 = s2*0.95
                v = v+s2
                # check for special use of 'never' as valence modifier instead of negation
                if wordsAndEmoticons[i-2] == "never" and (wordsAndEmoticons[i-1] == "so" or wordsAndEmoticons[i-1] == "this"): 
                    v = v*1.5                    
                # otherwise, check for negation/nullification
                elif negated([wordsAndEmoticons[i-2]]): v = v*n_scalar
            if i > 2 and str(wordsAndEmoticons[i-3]).lower() not in word_valence_dict:
                s3 = scalar_inc_dec(wordsAndEmoticons[i-3], v)
                if s3 != 0: s3 = s3*0.9
                v = v+s3
                # check for special use of 'never' as valence modifier instead of negation
                if wordsAndEmoticons[i-3] == "never" and \
                   (wordsAndEmoticons[i-2] == "so" or wordsAndEmoticons[i-2] == "this") or \
                   (wordsAndEmoticons[i-1] == "so" or wordsAndEmoticons[i-1] == "this"):
                    v = v*1.25
                # otherwise, check for negation/nullification
                elif negated([wordsAndEmoticons[i-3]]): v = v*n_scalar
                
                # check for special case idioms using a sentiment-laden keyword known to SAGE
                special_case_idioms = {"the shit": 3, "the bomb": 3, "bad ass": 1.5, "yeah right": -2, 
                                       "cut the mustard": 2, "kiss of death": -1.5, "hand to mouth": -2}
                # future work: consider other sentiment-laden idioms
                #other_idioms = {"back handed": -2, "blow smoke": -2, "blowing smoke": -2, "upper hand": 1, "break a leg": 2, 
                #                "cooking with gas": 2, "in the black": 2, "in the red": -2, "on the ball": 2,"under the weather": -2}
                onezero = "{} {}".format(str(wordsAndEmoticons[i-1]), str(wordsAndEmoticons[i]))
                twoonezero = "{} {} {}".format(str(wordsAndEmoticons[i-2]), str(wordsAndEmoticons[i-1]), str(wordsAndEmoticons[i]))
                twoone = "{} {}".format(str(wordsAndEmoticons[i-2]), str(wordsAndEmoticons[i-1]))
                threetwoone = "{} {} {}".format(str(wordsAndEmoticons[i-3]), str(wordsAndEmoticons[i-2]), str(wordsAndEmoticons[i-1]))
                threetwo = "{} {}".format(str(wordsAndEmoticons[i-3]), str(wordsAndEmoticons[i-2]))                    
                if onezero in special_case_idioms: v = special_case_idioms[onezero]
                elif twoonezero in special_case_idioms: v = special_case_idioms[twoonezero]
                elif twoone in special_case_idioms: v = special_case_idioms[twoone]
                elif threetwoone in special_case_idioms: v = special_case_idioms[threetwoone]
                elif threetwo in special_case_idioms: v = special_case_idioms[threetwo]
                if len(wordsAndEmoticons)-1 > i:
                    zeroone = "{} {}".format(str(wordsAndEmoticons[i]), str(wordsAndEmoticons[i+1]))
                    if zeroone in special_case_idioms: v = special_case_idioms[zeroone]
                if len(wordsAndEmoticons)-1 > i+1:
                    zeroonetwo = "{} {}".format(str(wordsAndEmoticons[i]), str(wordsAndEmoticons[i+1]), str(wordsAndEmoticons[i+2]))
                    if zeroonetwo in special_case_idioms: v = special_case_idioms[zeroonetwo]
                
                # check for booster/dampener bi-grams such as 'sort of' or 'kind of'
                if threetwo in booster_dict or twoone in booster_dict:
                    v = v+b_decr
            
            # check for negation case using "least"
            if i > 1 and str(wordsAndEmoticons[i-1]).lower() not in word_valence_dict \
                and str(wordsAndEmoticons[i-1]).lower() == "least":
                if (str(wordsAndEmoticons[i-2]).lower() != "at" and str(wordsAndEmoticons[i-2]).lower() != "very"):
                    v = v*n_scalar
            elif i > 0 and str(wordsAndEmoticons[i-1]).lower() not in word_valence_dict \
                and str(wordsAndEmoticons[i-1]).lower() == "least":
                v = v*n_scalar
        sentiments.append(v) 
            
    # check for modification in sentiment due to contrastive conjunction 'but'
    if 'but' in wordsAndEmoticons or 'BUT' in wordsAndEmoticons:
        try: bi = wordsAndEmoticons.index('but')
        except: bi = wordsAndEmoticons.index('BUT')
        for s in sentiments:
            si = sentiments.index(s)
            if si < bi: 
                sentiments.pop(si)
                sentiments.insert(si, s*0.5)
            elif si > bi: 
                sentiments.pop(si)
                sentiments.insert(si, s*1.5) 
                
    if sentiments:                      
        sum_s = float(sum(sentiments))
        #print sentiments, sum_s
        
        # check for added emphasis resulting from exclamation points (up to 4 of them)
        ep_count = str(text).count("!")
        if ep_count > 4: ep_count = 4
        ep_amplifier = ep_count*0.292 #(empirically derived mean sentiment intensity rating increase for exclamation points)
        if sum_s > 0:  sum_s += ep_amplifier
        elif  sum_s < 0: sum_s -= ep_amplifier
        
        # check for added emphasis resulting from question marks (2 or 3+)
        qm_count = str(text).count("?")
        qm_amplifier = 0
        if qm_count > 1:
            if qm_count <= 3: qm_amplifier = qm_count*0.18
            else: qm_amplifier = 0.96
            if sum_s > 0:  sum_s += qm_amplifier
            elif  sum_s < 0: sum_s -= qm_amplifier

        compound = normalize(sum_s)
        
        # want separate positive versus negative sentiment scores
        pos_sum = 0.0
        neg_sum = 0.0
        neu_count = 0
        for sentiment_score in sentiments:
            if sentiment_score > 0:
                pos_sum += (float(sentiment_score) +1) # compensates for neutral words that are counted as 1
            if sentiment_score < 0:
                neg_sum += (float(sentiment_score) -1) # when used with math.fabs(), compensates for neutrals
            if sentiment_score == 0:
                neu_count += 1
        
        if pos_sum > math.fabs(neg_sum): pos_sum += (ep_amplifier+qm_amplifier)
        elif pos_sum < math.fabs(neg_sum): neg_sum -= (ep_amplifier+qm_amplifier)
        
        total = pos_sum + math.fabs(neg_sum) + neu_count
        pos = math.fabs(pos_sum / total)
        neg = math.fabs(neg_sum / total)
        neu = math.fabs(neu_count / total)
        
    else:
        compound = 0.0; pos = 0.0; neg = 0.0; neu = 0.0
        
    s = {"neg" : round(neg, 3), 
         "neu" : round(neu, 3),
         "pos" : round(pos, 3),
         "compound" : round(compound, 4)}
    return s



from optparse import OptionParser

def checkText(text):
    analyse = sentiment(text)
    compound = analyse["compound"]
    if compound < -threshold:
        return -1
    elif compound > threshold:
        return 1
    else:
        return 0

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-f', '--filename', dest='filename')
    parser.add_option('-t', '--text', dest='text')
    options, args = parser.parse_args()

    if options.text:
        print checkText(options.text)

    elif options.filename:
        with open(options.filename) as F:
            for text in F:
                print checkText(text)





def pairs_aux(ngrams1, index1I, index1F, ngrams1_indexes_to_remove, ngrams2, index2I, index2F, ngrams2_indexes_to_remove):
    def aux_ngram(ngram):
        if type(ngram) == tuple:
            return ngram[0] + ' ' + ngram[1] #transform bigram list in 'unigram unigram' str
        elif type(ngram) == str:
            return ngram
    
    score = 0
    for i1 in range(index1I, index1F):
        for i2 in range (index2I + i1, index2F):
            pair = aux_ngram(ngrams1[i1]) + '---' + aux_ngram(ngrams2[i2])
            
            if pairs_lexicon.has_key(pair):
                score += pairs_lexicon.get(pair);
                #setting indexes to remove elements from bigrams and unigrams lists
                ngrams1_indexes_to_remove.add(i1)
                ngrams2_indexes_to_remove.add(i2)
    return score




# if __name__ == '__main__':
#     parser = OptionParser()
#     parser.add_option('-f', '--filename', dest='filename')
#     parser.add_option('-t', '--text', dest='text')
#     options, args = parser.parse_args()

#     if len(sys.argv) != 3:
#         print "Remeber the parameters: [-t|-f] ['text'|input_file]"
#         print "Finished"
#         os.environ["CURRENT_DIR"] = ""
#         os.environ["RULE_BASED_DIR"] = ""
#         sys.exit(0)

#     if options.text:
#         result = check_text(options.text)
#         print result

#     if options.filename:
#         with open(options.filename,"r") as file_obj:
#             for line in file_obj:
#                 result = check_text(line)
#                 print result