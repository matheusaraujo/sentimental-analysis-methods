#!/usr/bin/python
# -*- coding: utf-8 -*-
import imp
from multiprocessing import Process, Queue, Manager
from collections import OrderedDict
from subprocess import call, check_output
import os
import sys
from sasa.classifier import Classifier
from optparse import OptionParser
sasaClassifier = Classifier()

#sasa
def sasaSentiment(text):
    checkText = sasaClassifier.classifyFromText
    return checkText(text)[1]


current_dir_path = os.path.dirname(os.path.abspath(__file__))
upper_dir_path = "/".join(current_dir_path.split("/")[:-1])


def runEmoticons(dic,text):
    os.chdir(upper_dir_path + "/emoticons/")
    out = check_output("./emoticons.pl -t '" + text + "'",shell=True)
    out = out.replace("\n", "")
    dic["emoticons"] = float(out)

def runHappinessIndex(dic,text):
    os.chdir(upper_dir_path + "/happiness_index/")
    out = check_output(sys.executable + " happiness_index.py -t '" + text + "'",shell=True)
    out = out.replace("\n", "")
    dic["happiness"] = float(out)

def runSasa(dic,text):
    out = sasaSentiment(text)
    dic["sasa"] = float(out)

def runSentiwordnet(dic,text):
    os.chdir(upper_dir_path + "/sentiwordnet/")
    out = check_output(sys.executable +" sentiwordnet.py -t '" + text + "'",shell=True)
    out = out.replace("\n", "")
    dic["swn"] = float(out)

def runSentistrength(dic,text):
    os.chdir(upper_dir_path + "/sentistrength/")
    out = check_output(sys.executable +" sentistrength.py -t '" + text + "'",shell=True)
    out = out.replace("\n", "")
    dic["sentistrength"] = float(out)

def runPanas(dic,text):
    os.chdir(upper_dir_path + "/panas/")
    out = check_output("perl panas.pl -t '" + text + "'",shell=True)
    out = out.replace("\n", "")
    dic["panas"] = float(out)

def runSenticnet(dic,text):
    os.chdir(upper_dir_path + "/senticnet/")
    out = check_output(sys.executable +" senticnet.py -t '" + text + "'",shell=True)
    out = out.replace("\n", "")
    dic["senticnet"] = float(out)

# #Load all Methods
# os.chdir(upper_dir_path + "/senticnet/")
# check_senticnet = imp.load_source("checkText", senticnet_path).checkText
# os.chdir(upper_dir_path + "/happiness_index/")
# check_happiness = imp.load_source("checkText", happiness_path).checkText
# check_sasa = imp.load_source("checkText", sasa_path).checkText
# check_sentiwordnet = imp.load_source("checkText", sentiwordnet_path).checkText
# check_sentistrength = imp.load_source("checkText", sentistrength_path).checkText

# def check_emoticons(text):
#     emoticons_cmd = ["perl", emoticons_path, "-t", "\"" + text + "\""]
#     output = check_output(emoticons_cmd)
#     return int(output)

# def check_panas(text):
#     panas_cmd = ["perl", panas_path, "-t", "\"" + text + "\""]
#     output = check_output(panas_cmd)
#     return int(output)

# def caller_function(dic,function,metodo,text):
#     output = function(text)
#     dic[metodo] = float(output)

#Constant
default_weights = {
"swn":0.655,
"emoticons":0.853,
"panas":0.629,
"sasa":0.646,
"happiness":0.662,
"senticnet":0.653,
"sentistrength":0.781
}


def combinedCheckText(text):
        text = text.replace("\"","").replace("\'","")
        manager = Manager()
        dic = manager.dict()
        
        #Paralelized calls
        p_emoticon = Process(target=runEmoticons, args=(dic,text,)) 
        p_panas = Process(target=runPanas, args=(dic,text,)) 
        p_happiness = Process(target=runHappinessIndex, args=(dic,text,)) 
        p_sentistrength = Process(target=runSentistrength, args=(dic,text,)) 
        p_senticnet = Process(target=runSenticnet, args=(dic,text,))
        p_sasa = Process(target=runSasa, args=(dic,text)) 

        if not options.quick:
            p_sentiwordnet = Process(target=runSentiwordnet, args=(dic,text,)) 
        
        #Start Process
        p_emoticon.start()
        p_panas.start()
        p_happiness.start()
        p_sentistrength.start()
        p_senticnet.start()
        p_sasa.start()

        if not options.quick:        
            p_sentiwordnet.start()

        #Join Process 
        p_emoticon.join()
        p_panas.join()
        p_happiness.join()
        p_sentistrength.join()
        p_senticnet.join()
        p_sasa.join()

        if not options.quick:
            p_sentiwordnet.join()
        


        #Get Combined Method Result
        combined_method = 0
        sum_weights = 0
        weights = default_weights
        dicItems = dic.items()
        for k,v in dicItems:
            if k == "swn"  and options.quick:
                continue
            if v == 0:
                continue
            if v > 0:
                combined_method += (1*weights[k])
            if v < 0:
                combined_method += (-1*weights[k])
            sum_weights += weights[k]
        
        try:
            dic["combined"] = combined_method/float(sum_weights)

        except ZeroDivisionError:
            dic["combined"] = 0

        return dic["combined"]
 



if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option('-q', help="Quick Avaliation", action="store_true", dest='quick',default=False)
    parser.add_option('-f', '--file', dest='file')
    parser.add_option('-t', '--text', dest='text')
    options, args = parser.parse_args()
    if options.text:       
        print combinedCheckText(options.text)

    elif options.file:
        with open(options.file,"rb") as infile:
            for line in infile:
                print combinedCheckText(line)
    else:
        print "Remember: [-t|-f] [text | input file]"
