#!/usr/bin/perl -w
	use Time::Local;
	use HTML::Filter;
	use HTML::Entities;
	use Lingua::StopWords qw( getStopWords );
	use List::Util qw/max/;
	
	my $arg1; #-t ou -f
	my $arg2; #tweet ou arquivo
	my $c = 0;
	our $text;
	
	    my $happy = 1;
    my $very_happy = 1;
    my $very_sad = -1;
    my $sad = -1;
    my $angry = -1;
    my $thinking = 0;
    my $surprised = 1;
    my $crying = -1;
    my $tears_happy = 1;
    my $secret = 0;
    my $excited = 1;
    my $love = 1;
    my $joyful = 1;
    my $suspicious = -1;
    my $tongue = 1;
    my $troubled = -1;
    my $kiss = 1;
    my $confused = -1;
    my $disappointed = -1;
    my $wink = 1;
    my $embarrassed = -1;

  	my $totalNeg = 0;
  	my $totalPos = 0;
	
	my $neut = 0;
	
	#extrai informacoes dos parametros passados na linha de comando
	foreach $argnum (0 .. $#ARGV) 
	{
		if($c == 0)
		{
			$arg1 = $ARGV[$argnum];
			$c+=1;
		}
		elsif($c == 1)
		{
			$arg2 = $ARGV[$argnum];
			$c+=1;
		}
		#print "$ARGV[$argnum]\n";
	}
	
	if($arg1 eq "-t")
	{
		$text = $arg2;
		&trataTweet;
	}
	elsif($arg1 eq "-f")
	{
		&abreArqs;
	}
    

	
	sub trataTweet
	{
		$text =~ s/(www|http|https|ftp|gopher|telnet|file|notes|ms-help)(\S+)+/ /g; #elimina www.(qualquercoisa)
				
				if($text =~ /(:|=)(\)|\]|\})/ or $text =~ /(:|=)(o|O|0|\-|\^)(\)|\]|\})/)
				{
					$totalPos += 1;

				}
				if($text =~ /(:|=)(D|B)/ or $text =~ /(:|=)(o|O|0|\-|\^)(D|B)/)
				{
					$totalPos += 1;

				}
				if($text =~ /(D)(:|=)/ or $text =~ /(D)(\-|\^)(:|=)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(\(|\[|\{|c|C|&amp;lt;)/ or $text =~ /(:|=)(o|O|0|\-|\^)(\(|\[|\{|c|C|&amp;lt;)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(&amp;gt;)(:|=)(\(|\[|\{|c|C|&amp;lt;|\||\\|\/)/ or $text =~ /(&amp;gt;)(:|=)(o|O|0|\-|\^)(\(|\[|\{|c|C|&amp;lt;|\||\\|\/)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(\|)/ or $text =~ /(:|=)(o|O|0|\-|\^)(\|)/)
				{
				}
				if($text =~ /(:|=)(o|O|0)/ or $text =~ /(:|=)(\-|\^)(o|O|0)/ or $text =~ /(8)(\-|\_|\.)(o|O|0|@)/)
				{
					$totalPos += 1;

				}
				if($text =~ /(:|=)(')(\(|\[|\{|c|C|&amp;lt;)/ or $text =~ /(:|=)(')(\-|\^)(\(|\[|\{|c|C|&amp;lt;)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(')(\)|\}|\}|&amp;gt;)/ or $text =~ /(:|=)(')(\-|\^)(\)|\}|\}|&amp;gt;)/)
				{
					$totalPos += 1;

				}
				if($text =~ /(:|=)(x|X|#)/ or $text =~ /(:|=)(\-|\^)(x|X|#)/)
				{

				}
				if($text =~ /(\\|\_|\|)(o|O|0)(\/|\_|\|)/)
				{
					$totalPos += 1;
								   
				}
				if($text =~ /(&amp;lt;)(3)/)
				{
					$totalPos += 1;
				}
				if($text =~ /(\^)(\^)/ or $text =~ /(\^)(\.|\-|\_)(\^)/)
				{
					$totalPos += 1;
					
				}
				if($text =~ /(&not;)(&not;)/ or $text =~ /(&not;)(\.|\_)(&not;)/)
				{
					$totalNeg += 1;
					
				}
				if($text =~ /(:|=)(P|p|b)/ or $text =~ /(:|=)(o|O|0|\-|\^)(P|p|b)/)
				{
					$totalPos += 1;

					
				}
				if($text =~ /(\&amp;gt;)(\&amp;lt;)/ or $text =~ /(\&amp;gt;)(\.|\_)(\&amp;lt;)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(\*)/ or $text =~ /(:|=)(\-|\^)(\*)/)
				{
					$totalPos += 1;
					
				}
				if($text =~ /(o|O|0|@)(\-|\_|\.)(o|O|0|@)/)
				{
					$totalNeg += 1;
					
				}
				if($text =~ /(:|=)(\\|\/)/ or $text =~ /(:|=)(\-|\^)(\\|\/)/)
				{
					$totalNeg += 1;
					
				}
				if($text =~ /(;)(\)|\]|\})/)
				{
					$totalPos += 1;
					
				}
				if($text =~ /(:|=)($)/ or $text =~ /(:|=)(\-|\^)($)/)
				{
					$totalNeg += 1;
					
				}
				if($totalPos > $totalNeg)
				{
					print "1\n";
				}
				elsif($totalPos < $totalNeg)
				{
					print "-1\n";
				}
				else
				{
					print "$neut\n";
				}
	}
    
	sub abreArqs
	{
		#our $event = "beijim_end.txt.gz";
		#our $saida = "saida_emoticons.txt.gz";
		#our $dirlog = "/home/matheus/twiter_events/eventos_filtered";
		#our $datalog = "/home/pollyanna/Projeto/Emoticons/Nova_Saida";
		
		open(IN, "$arg2 ") or die "could not open $arg2\n";
		#open(OUT1, "| gzip -c > $datalog/$saida") || die "Cannot create $datalog/$saida\n";  
		
		while (my $line = <IN>) 
		{
			chomp $line;
			
			#if($line =~ /(\S+) (\S+) (\S+) (\S+) <d>([^<]+)<\/d> <s>([^<]+)<\/s> <t>([^<]+)<\/t> (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) <n>([^<]+)<\/n> <ud>([^<]+)<\/ud> <t>([^<]+)<\/t> <l>([^<]+)<\/l>/)
			#{
				#my $text = $7; 
				$text = $line;				
				
				$text =~ s/(www|http|https|ftp|gopher|telnet|file|notes|ms-help)(\S+)+/ /g; #elimina www.(qualquercoisa)
				
				if($text =~ /(:|=)(\)|\]|\})/ or $text =~ /(:|=)(o|O|0|\-|\^)(\)|\]|\})/)
				{
					$totalPos += 1;

				}
				if($text =~ /(:|=)(D|B)/ or $text =~ /(:|=)(o|O|0|\-|\^)(D|B)/)
				{
					$totalPos += 1;

				}
				if($text =~ /(D)(:|=)/ or $text =~ /(D)(\-|\^)(:|=)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(\(|\[|\{|c|C|&amp;lt;)/ or $text =~ /(:|=)(o|O|0|\-|\^)(\(|\[|\{|c|C|&amp;lt;)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(&amp;gt;)(:|=)(\(|\[|\{|c|C|&amp;lt;|\||\\|\/)/ or $text =~ /(&amp;gt;)(:|=)(o|O|0|\-|\^)(\(|\[|\{|c|C|&amp;lt;|\||\\|\/)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(\|)/ or $text =~ /(:|=)(o|O|0|\-|\^)(\|)/)
				{
				}
				if($text =~ /(:|=)(o|O|0)/ or $text =~ /(:|=)(\-|\^)(o|O|0)/ or $text =~ /(8)(\-|\_|\.)(o|O|0|@)/)
				{
					$totalPos += 1;

				}
				if($text =~ /(:|=)(')(\(|\[|\{|c|C|&amp;lt;)/ or $text =~ /(:|=)(')(\-|\^)(\(|\[|\{|c|C|&amp;lt;)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(')(\)|\}|\}|&amp;gt;)/ or $text =~ /(:|=)(')(\-|\^)(\)|\}|\}|&amp;gt;)/)
				{
					$totalPos += 1;

				}
				if($text =~ /(:|=)(x|X|#)/ or $text =~ /(:|=)(\-|\^)(x|X|#)/)
				{

				}
				if($text =~ /(\\|\_|\|)(o|O|0)(\/|\_|\|)/)
				{
					$totalPos += 1;
								   
				}
				if($text =~ /(&amp;lt;)(3)/)
				{
					$totalPos += 1;
				}
				if($text =~ /(\^)(\^)/ or $text =~ /(\^)(\.|\-|\_)(\^)/)
				{
					$totalPos += 1;
					
				}
				if($text =~ /(&not;)(&not;)/ or $text =~ /(&not;)(\.|\_)(&not;)/)
				{
					$totalNeg += 1;
					
				}
				if($text =~ /(:|=)(P|p|b)/ or $text =~ /(:|=)(o|O|0|\-|\^)(P|p|b)/)
				{
					$totalPos += 1;

					
				}
				if($text =~ /(\&amp;gt;)(\&amp;lt;)/ or $text =~ /(\&amp;gt;)(\.|\_)(\&amp;lt;)/)
				{
					$totalNeg += 1;

				}
				if($text =~ /(:|=)(\*)/ or $text =~ /(:|=)(\-|\^)(\*)/)
				{
					$totalPos += 1;
					
				}
				if($text =~ /(o|O|0|@)(\-|\_|\.)(o|O|0|@)/)
				{
					$totalNeg += 1;
					
				}
				if($text =~ /(:|=)(\\|\/)/ or $text =~ /(:|=)(\-|\^)(\\|\/)/)
				{
					$totalNeg += 1;
					
				}
				if($text =~ /(;)(\)|\]|\})/)
				{
					$totalPos += 1;
					
				}
				if($text =~ /(:|=)($)/ or $text =~ /(:|=)(\-|\^)($)/)
				{
					$totalNeg += 1;
					
				}
				if($totalPos > $totalNeg)
				{
					print "1\n";
				}
				elsif($totalPos < $totalNeg)
				{
					print "-1\n";
				}
				else
				{
					print "$neut\n";
				}

				$totalPos = 0;
				$totalNeg = 0;

			#}
		}
	}
       
    #close OUT1;
