import nltk
import os
from optparse import OptionParser
import sys

CURRENT_FILE_PATH = os.path.dirname(__file__)
lexicon = {}
stopwords = nltk.corpus.stopwords.words("english")

if CURRENT_FILE_PATH != "":
    CURRENT_FILE_PATH += "/"

def load_lexicon():
	tab_tokenizer = nltk.tokenize.TabTokenizer().tokenize
	database_filename = CURRENT_FILE_PATH + "emoticon.words.advanced"
	with open(database_filename,"r") as file_ptr:
		for line in file_ptr:
			line = line.rstrip()
			tokens = tab_tokenizer(line)
			positive = int(tokens[0])
			negative = int(tokens[1])
			if positive > negative:
				lexicon[tokens[2]] = 1
			elif positive < negative:
				lexicon[tokens[2]] = -1
			else:
				lexicon[tokens[2]] = 0

def checkText(text):
	text  = text.lower()
	white_tokenizer = nltk.tokenize.WhitespaceTokenizer().tokenize
	words = white_tokenizer(text)
	words_clean = []
	for word in words:
		if word not in stopwords:
			words_clean.append(word)
	pos = 0
	neg = 0
	neu = 0

	for word in words_clean:
		if lexicon.has_key(word):
			if lexicon[word] > 0:
				pos += 1
			elif lexicon[word] < 0:
				neg += 1
			else:
				neu += 1
	if pos > neg:
		return 1
	elif neg > pos:
		return -1
	else:
		return 0


load_lexicon()
if __name__ == '__main__':
	parser = OptionParser()
	parser.add_option('-f', '--filename', dest='filename')
	parser.add_option('-t', '--text', dest='text')
	options, args = parser.parse_args()

	if len(sys.argv) != 3:
		print "Remeber the parameters: [-t|-f] ['text'|input_file]"
		#Clean Enviroment
		print "Finished"
		os.environ["CURRENT_DIR"] = ""
		os.environ["RULE_BASED_DIR"] = ""
		sys.exit(0)

	if options.text:
		result = checkText(options.text)
		print result

	if options.filename:
		with open(options.filename,"r") as file_obj:
			for line in file_obj:
				result = checkText(line)
				print result