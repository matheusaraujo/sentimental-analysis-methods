import csv
import os
path_dir = os.path.dirname(os.path.abspath(__file__))
anew_file = os.path.join(path_dir,"new_anew.csv")

ANEW_set = {}

class anew_word():
    def __init__(self,word,val_mn):
        self.word   = word
        self.val_mn = float(val_mn)

with open(anew_file,"rb") as csvfile:
    reader = csv.reader(csvfile,delimiter=",")
    for row in reader:
        word = row[0]
        ANEW_set[word] = anew_word(word,row[1])
