#!/usr/bin/perl -w
use strict;

open(IN1, "negative-words.txt") or die "could not open negative-words.txt\n";
open(IN2, "positive-words.txt") or die "could not open positive-words.txt\n";

our %positive = ();
our %negative = ();

while (my $line = <IN1>) 
{
    chomp $line;
    
    if(not exists $negative{$line})
    {
        $negative{$line} = 1;
    }
}

while (my $line = <IN2>) 
{
    chomp $line;
    
    if(not exists $positive{$line})
    {
        $positive{$line} = 1;
    }
}

my $arg1; #-t ou -f
my $arg2; #tweet ou arquivo
my $c = 0;
our $text;

foreach my $argnum (0 .. $#ARGV)
{
    if($c == 0)
    {
         $arg1 = $ARGV[$argnum];
         $c+=1;
    }
    elsif($c == 1)
    {
        $arg2 = $ARGV[$argnum];
        $c+=1;
    }
}

if($arg1 eq "-t")
{
    $text = $arg2;
    &trataTweet;
 }
elsif($arg1 eq "-f")
{
    &abreArqs;
}

sub trataTweet
{
    my @words = split(" ", $text);
        
        my $pos;
        my $neg;
        
        my $pos_total = 0;
        my $neg_total = 0;
        my $neut_total = 0;
        
        foreach(@words)
        {	
	    my $w = $_;
            if(exists $negative{$w})
            {          
                $neg_total += 1;
            }
            elsif(exists $positive{$w})
            {
                $pos_total += 1;
            }
        }
        
        if($pos_total > $neg_total)
        {
            print "1\n";
        }
        
        elsif($neg_total > $pos_total)
        {
            print "-1\n";
        }
        
        else
        {
            print "0\n";
        }
}

sub abreArqs
{
    open(IN3, "$arg2") or die "could not open $arg2\n";
    
    while (my $line = <IN3>) 
    {
        chomp $line;
        
        my @words = split(" ", $line);
        
        my $pos;
        my $neg;
        
        my $pos_total = 0;
        my $neg_total = 0;
        my $neut_total = 0;
        
        foreach(@words)
        {
            if(exists $negative{$_})
            {          
                $neg_total += 1;
            }
            elsif(exists $positive{$_})
            {
                $pos_total += 1;
            }
        }
        
        if($pos_total > $neg_total)
        {
            print "1\n";
        }
        
        elsif($neg_total > $pos_total)
        {
            print "-1\n";
        }
        
        else
        {
            print "0\n";
        }
        
    }
}

close IN1;
close IN2;
close IN3;


