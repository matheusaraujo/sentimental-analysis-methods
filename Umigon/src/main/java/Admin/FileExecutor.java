package Admin;
import Classifier.TweetLooper;
import Twitter.ExternalSourceTweetLoader;
import Twitter.Tweet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

/**
 * Created by matheus on 4/20/15.
 */
public class FileExecutor {

    public static TweetLooper hl1 = new TweetLooper();
    public static ExternalSourceTweetLoader comp = new ExternalSourceTweetLoader();

    public static void printSentiment(List<Tweet> listTweets){
        int output;
        try {
            //Open Output File
            listTweets = hl1.applyLevel1(listTweets);
            for(Tweet tweet : listTweets){
                if (tweet.getListCategories().contains("011")){
                   output = 1;
                } else if (tweet.getListCategories().contains("012")){
                    output = -1;
                } else {
                    output = 0;
                }

                if(tweet.getListCategories().contains("022")){
                    output = output*2;
                }
                System.out.println(output);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String [] args){
        FileWriter fileW;
        FileReader file;
        BufferedReader buf;
        String inputString = null;
        String line;
        List<Tweet> listTweets;

        if(args.length == 0){
            System.out.println("Remeber the parameters: [-t|-f] ['text'|input_file]");
            System.exit(0);
        }

        if (args[0].contains("-f")){
            //Read Input File and put in inputString
            try {
                file = new FileReader(args[1]);
                buf = new BufferedReader(file);
                line = buf.readLine();
                while(line != null){
                    if (inputString == null){
                        inputString = line;
                    } else{
                        inputString = inputString + "\n" + line;
                    }

                    line = buf.readLine();
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            //Create List of Tweets
            listTweets = comp.userInputTweets(inputString);

            //Executing Algorithm
            printSentiment(listTweets);

        } else if(args[0].contains("-t")){
            inputString = args[1];
            listTweets = comp.userInputTweets(inputString);
            //Executing Algorithm
            printSentiment(listTweets);

        }else{
            System.out.println("Remeber the parameters: [-t|-f] ['text'|input_file]");
        }

    }
}
