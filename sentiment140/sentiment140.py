import nltk
import os
import sys
from optparse import OptionParser

#TODO remover caracteres de pontuacao no final de cada token, por exemplo usando a ER [,.!?]

unigrams_lexicon = {} #key: a str representing an unigram; value: the unigram's score
bigrams_lexicon = {}  #key: a str representing a bigram, in format 'unigram<space>unigram'; values: the bigram's score
pairs_lexicon = {}    #key: a str representing a pair of non-contiguous ngrams, in format 'ngram---ngram', where ngram can be unigram or bigram; value: the pair's score
THRESHOLD_NEUTRAL = 0.5 #absolute value
CURRENT_FILE_PATH = os.path.dirname(__file__)

if CURRENT_FILE_PATH != "":
    CURRENT_FILE_PATH += "/"

def load_lexicons():
    load_lexicon(unigrams_lexicon, 'unigrams')
    load_lexicon(bigrams_lexicon, 'bigrams')
    load_lexicon(pairs_lexicon, 'pairs')

def load_lexicon(ngrams_lexicon, ngrams_file_prefix):
    tab_tokenizer = nltk.tokenize.TabTokenizer().tokenize
    database_filename = CURRENT_FILE_PATH + ngrams_file_prefix + "-pmilexicon.txt"
    with open(database_filename,"r") as file_ptr:
        for line in file_ptr:
            line = line.rstrip()
            tokens = tab_tokenizer(line)
            ngrams_lexicon[tokens[0]] = float(tokens[1])


def check_text(text):
    text  = text.lower()
    white_tokenizer = nltk.tokenize.WhitespaceTokenizer().tokenize
    words = white_tokenizer(text)
    score_total = 0.0

    unigrams_indexes_to_remove = set()
    bigrams_indexes_to_remove = set()

    unigrams = words;
    bigrams = nltk.bigrams(words)
    qtd_u = unigrams.__len__()
    qtd_b = bigrams.__len__()

    #pairs unigram-unigram
    score_total += pairs_aux(unigrams, 0, qtd_u - 2, unigrams_indexes_to_remove, unigrams, 2, qtd_u, unigrams_indexes_to_remove)
    #pairs unigram-bigram
    score_total += pairs_aux(unigrams, 0, qtd_u - 2, unigrams_indexes_to_remove, bigrams, 2, qtd_b, bigrams_indexes_to_remove)
    #pairs bigram-unigram
    score_total += pairs_aux(bigrams, 0, qtd_b - 2, bigrams_indexes_to_remove, unigrams, 3, qtd_u, unigrams_indexes_to_remove)
    #pairs bigram-bigram
    score_total += pairs_aux(bigrams, 0, qtd_b - 2, bigrams_indexes_to_remove, bigrams, 3, qtd_b, bigrams_indexes_to_remove)

    #remove bigrams
    for idx in bigrams_indexes_to_remove:
        bigrams[idx] = None

    #bigrams
    i = 0
    for bigram in bigrams:
        if bigram != None:
            bigram_str = bigram[0] + ' ' + bigram[1]

            if bigrams_lexicon.has_key(bigram_str):
                score_total += bigrams_lexicon[bigram_str]
                unigrams_indexes_to_remove.add(i)
        i += 1

    #remove unigrams
    for idx in unigrams_indexes_to_remove:
        unigrams[idx] = None

    #unigrams
    for unigram in unigrams:
        if unigram != None and unigrams_lexicon.has_key(unigram):
            score_total += unigrams_lexicon[unigram]

    if score_total > THRESHOLD_NEUTRAL:
        return 1 #positive
    elif score_total < -THRESHOLD_NEUTRAL:
        return -1 #negative
    else:
        return 0 #neutral OR N/A

def checkText(text):
    return check_text(text)

def pairs_aux(ngrams1, index1I, index1F, ngrams1_indexes_to_remove, ngrams2, index2I, index2F, ngrams2_indexes_to_remove):
    def aux_ngram(ngram):
        if type(ngram) == tuple:
            return ngram[0] + ' ' + ngram[1] #transform bigram list in 'unigram unigram' str
        elif type(ngram) == str:
            return ngram
    
    score = 0
    for i1 in range(index1I, index1F):
        for i2 in range (index2I + i1, index2F):
            pair = aux_ngram(ngrams1[i1]) + '---' + aux_ngram(ngrams2[i2])
            
            if pairs_lexicon.has_key(pair):
                score += pairs_lexicon.get(pair);
                #setting indexes to remove elements from bigrams and unigrams lists
                ngrams1_indexes_to_remove.add(i1)
                ngrams2_indexes_to_remove.add(i2)
    return score


load_lexicons()

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-f', '--filename', dest='filename')
    parser.add_option('-t', '--text', dest='text')
    options, args = parser.parse_args()

    if len(sys.argv) != 3:
        print "Remeber the parameters: [-t|-f] ['text'|input_file]"
        print "Finished"
        os.environ["CURRENT_DIR"] = ""
        os.environ["RULE_BASED_DIR"] = ""
        sys.exit(0)

    if options.text:
        result = check_text(options.text)
        print result

    if options.filename:
        with open(options.filename,"r") as file_obj:
            for line in file_obj:
                result = check_text(line)
                print result