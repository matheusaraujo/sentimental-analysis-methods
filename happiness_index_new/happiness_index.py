#!/usr/bin/python
import imp
import os
from optparse import OptionParser
import gzip

CURRENT_FILE_PATH = os.path.dirname(__file__)
if CURRENT_FILE_PATH != "":
    CURRENT_FILE_PATH += "/"
#import ANEW_set
ANEW_set = imp.load_source("ANEW_set", CURRENT_FILE_PATH + "anew_to_dict.py").ANEW_set

def getWordFreq(text):
    words = text.replace("."," ").replace("!"," ").replace("?"," ").replace(","," ").lower().split(" ")
    word_freq = {}
    for word in words:
        if word_freq.has_key(word):
            word_freq[word] += 1
        else:
            word_freq[word] = 1
    return word_freq

def checkText(text):
    word_freq = getWordFreq(text)
    words = word_freq.keys()
    valence = 0
    numerator = 0
    sum_freq = 0
    for word in words:
        if ANEW_set.has_key(word):
            sum_freq += word_freq[word]
            anew_val = ANEW_set[word].val_mn
            numerator += anew_val * word_freq[word]
    if sum_freq == 0:
        return 0
    happiness_index = numerator/sum_freq
    sentiment = (happiness_index - 5)/4.
    return sentiment

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-f', '--file', dest='filename')
    parser.add_option('-p', '--just_polarity',action="store_true", dest='just_polarity', default=False)
    parser.add_option('-z', '--gzip file',action="store_true", dest='gzip_file', default=False)
    parser.add_option('-t', '--text', dest='text')
    parser.add_option('--human', action="store_true", dest='human', default=False)
    parser.add_option('-o', '--out', dest='out')

    options, args = parser.parse_args()

    #Just Text
    if options.text:
        sentiment = checkText(options.text)
        #Happinesse index Normalization
        print sentiment


    #Just File
    if options.filename:
        with open(options.filename,"r") as fp:
            for line in fp:
                line = line.replace("\n","")
                sentiment = checkText(line)
                #Happinesse index Normalization
                print sentiment